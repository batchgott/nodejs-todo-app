import chai from "chai";
import chaiHttp from "chai-http";
import app from "../server";
import { Item } from "../src/model/item.model";
import { createItemErr,createItemSuc, findItemSuc, updateItemErr,updateItemSuc,deleteItemSuc } from "./testItems";

chai.use(chaiHttp);
chai.should();
var user={};
describe('Clear Items', () => {
    beforeEach((done) => {
        Item.deleteMany({}, (err) => {   
            chai.request(app)
            .post("/api/login")
            .send({
                "username":"user2",
                "password":"123456"
            })
            .end((err,res)=>{
                setTimeout(()=>{},1000);
                console.log(res.body);
                user=res.body;
                done();
            })        
        });        
    });

describe("GET /api/items",()=>{
    it("should find all items",(done)=>{
        chai.request(app)
        .get("/api/items")
        .set("x-app-token",user.token)
        .end((err,res)=>{
            res.should.have.status(200);
            res.body.should.be.a('array');
            res.body.length.should.be.eql(0);
            done();
        });
    })
});

describe("GET /api/items/:id",()=>{
    it("should find a item by given id",(done)=>{
        var item=new Item(findItemSuc);
        item.save((err,item)=>{
            chai.request(app)
            .get("/api/items/"+item._id)
            .set("x-app-token",user.token)
            .end((err,res)=>{
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('title');
                res.body.should.have.property('description');
                res.body.should.have.property('done');
                res.body.should.have.property('date');
                res.body.should.have.property('_id').eql(item.id);
                done();
            });
        });
    });
});

describe('POST /api/items',()=>{
    it('should not create new item',(done)=>{
        chai.request(app)
        .post('/api/items')
        .set("x-app-token",user.token)
        .send(createItemErr).end((err,res)=>{
            res.should.have.status(400);
            res.body.should.be.a('object');
            res.body.should.have.property('error');
            res.body.error.should.be.eql("\"description\" is required");
            done();
        });
    });

    it('should create new item',(done)=>{
        chai.request(app)
        .post('/api/items')
        .set("x-app-token",user.token)
        .send(createItemSuc).end((err,res)=>{
            res.should.have.status(201);
            res.body.should.be.a('object');
            res.body.should.have.property('_id');
            res.body.should.have.property('title');
            res.body.should.have.property('description');
            res.body.should.have.property('done');
            res.body.should.have.property('date');
            done();
        });
    });
});

describe('PUT /api/items/:id',()=>{
    it('should not update item',(done)=>{
        let item=new Item(updateItemErr);
        item.save((err,item)=>{
            chai.request(app)
            .put('/api/items/'+item._id)
            .set("x-app-token",user.token)
            .send({
                _id:item._id,
                title:item.title,
                done:item.done,
                date:item.date
            }).end((err,res)=>{
                res.should.have.status(400);
                res.body.should.be.a('object');
                res.body.should.have.property('error');
                res.body.error.should.be.eql("\"description\" is required");
                done();
            });
        })
    });

    it('should update item',(done)=>{
        let item=new Item(updateItemSuc);
        item.save((err,item)=>{
            chai.request(app)
            .put('/api/items/'+item._id)
            .set("x-app-token",user.token)
            .send({
                _id:item._id,
                title:"New Title",
                description:item.description,
                done:false,
                date:item.date
            }).end((err,res)=>{
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('_id').eql(item.id);
                res.body.should.have.property('title').eql("New Title");
                res.body.should.have.property('description').eql(item.description);
                res.body.should.have.property('done').eql(false);
                res.body.should.have.property('date');
                done();
            });
        })
    });
});

describe('DELETE /api/items/:id',()=>{
    it("should delete item",(done)=>{
        let item=new Item(deleteItemSuc);
        item.save((err,item)=>{
            chai.request(app)
            .delete('/api/items/'+item._id)
            .set("x-app-token",user.token)
            .end((err,res)=>{
                res.should.have.status(204);
            done();
            });
        });
    });
});
})