export const createItemSuc={
        title:"Test Title1",
        description:"Some Desciption",
        done:true
    }

export const createItemErr={
        title:"Test Title2",
        done:false
    }

export const findItemSuc={
        title:"Test Title3",
        description:"Some Desciption",
        done:true
}

export const updateItemSuc={
    title:"Test Title4",
    description:"Some Desciption",
    done:true
}

export const updateItemErr={
    title:"Test Title5",
    description:"Some Desciption",
    done:false
}

export const deleteItemSuc={
    title:"Test Title6",
    description:"Some Desciption",
    done:false
}