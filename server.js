import express from 'express';
import ItemRouter from "./src/routes/item.routes";
import {configureDatabase, logRequest, logError, auth, seedUsers} from "./config";
import * as AuthContoller from "./src/controller/auth.controller"
import cors from "cors";
require('dotenv').config();

const app = express();

configureDatabase();

app.use(cors());
app.use(express.json());
app.use(logRequest);
app.use(logError);
seedUsers();

const router=express.Router();
router.use("/items",auth,ItemRouter);
router.post("/login",AuthContoller.login);
app.use("/api",router);


app.listen(3000);
console.log('app running on port http://127.0.0.1:3000',);

export default app;