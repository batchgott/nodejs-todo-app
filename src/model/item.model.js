import mongoose from "mongoose";

const ItemSchema=mongoose.Schema({
    title:{
        type:String,
        required:true,
    },
    description:{
        type: String,
        required: true
    },
    done:{
        type:Boolean,
        required:true,
        default:false
    },
    date:{
        type:Date,
        required:true,
        default: new Date(Date.now())
    }
});

export var Item=mongoose.model('Items',ItemSchema);
