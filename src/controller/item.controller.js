import {Item} from "../model/item.model";
import {createItemValidation, updateItemValidation} from "./validation/item.validation";
import {ObjectId} from "bson";

export const create=async (req,res)=>{
    const {error}=createItemValidation(req.body);
    if (error) { return res.status(400).json({error:error.details[0].message}); }
    const item=new Item({
       title:req.body.title,
        description:req.body.description,
        done:req.body.done
    });
    try{
        await item.save();
        res.status(201).json(item);
    }catch (err) {
        res.status(400).json({error:err})
    }
};

export const findAll=async (req,res)=>{
    try {
        var items=await Item.find();
        if(req.query.sortBy){
            if(req.query.sortBy==="date")
            items.sort((a,b)=>b.date-a.date);
            if(req.query.sortBy==="done")
            items.sort((a,b)=>
            (a.done===b.done)?
            0:a.done?
                -1:1);
        }
        res.status(200).json(items);
    } catch (err) {
        res.status(400).json({error:err})
    }
};

export const findOne=async(req,res)=>{
    try {
        const item=await Item.findById(req.params.id);
        res.status(200).json(item);
    } catch (err) {
        res.status(400).json({error:err})
    }
};

export const update=async(req,res)=>{
    const {error}=updateItemValidation(req.body);
    if (error) { return res.status(400).json({error:error.details[0].message}); }

    try {
        await Item.findByIdAndUpdate(req.params.id,{
            title:req.body.title,
            description:req.body.description,
            done:req.body.done,
            date:req.body.date
        });
        const item=await Item.findById(req.params.id);
        res.status(200).json(item);
    } catch (err) {
        res.status(400).json({error:err})
    }
};

export const remove=async(req,res)=>{
    try {
        await Item.deleteOne({_id: new ObjectId(req.params.id)});
        res.status(204).send();
    } catch (err) {
        res.status(400).json({error:err})
    }
};

export const toggleDone=async(req,res)=>{
    try {
        var item=await Item.findById(req.params.id);
        await Item.findByIdAndUpdate(req.params.id,{done:!item.done,});
        item = await Item.findById(req.params.id);
        res.status(200).json(item);
    } catch (err) {
        res.status(400).json({error:err})
    }
}
