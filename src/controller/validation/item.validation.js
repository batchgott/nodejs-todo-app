import Joi from "joi";
Joi.objectId = require('joi-objectid')(Joi)

export const createItemValidation=item=>{
    const schema={
        title:Joi.string().required(),
        description:Joi.string().required(),
        done:Joi.boolean().optional()
    }
    return Joi.validate(item,schema);
}

export const updateItemValidation=item=>{
    const schema={
        _id:Joi.objectId().required(),
        title:Joi.string().required(),
        description:Joi.string().required(),
        done:Joi.boolean().required(),
        date:Joi.date().required(),
        __v:Joi.optional()
    }
    return Joi.validate(item,schema);
}
