import jwt from "jsonwebtoken";
import {User} from "../model/user.model";
import bcrypt from "bcryptjs";

export const login=async(req,res)=>{
    try {
        const user = await User.findOne({username: req.body.username});
        if (!user) return res.status(400).json({error:"Username does not exist"}); 
    
        const validPassword = await bcrypt.compare(req.body.password, user.password);
        if (!validPassword) return res.status(400).json({error:"Invalid password"}); 
    
        const token = jwt.sign({_id: user._id}, process.env.TOKEN_SECRET,{expiresIn:"1h"});
        res.header("x-app-token", token).json({
            "_id":user._id,
            "username":user.username,
            "password":user.password,
            "token":token,
            "expirationDate":new Date(new Date().getTime()+ 3600000)
        });
    } catch (err) {
        res.status(400).json({error:err});
    }
}