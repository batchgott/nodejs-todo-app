import express from "express";
import * as ItemController from "../controller/item.controller"

const router = express.Router();

router.post("/",ItemController.create);
router.get("/",ItemController.findAll);
router.get("/:id",ItemController.findOne);
router.put("/:id",ItemController.update);
router.delete("/:id",ItemController.remove);
router.patch("/:id",ItemController.toggleDone);

export default router;
