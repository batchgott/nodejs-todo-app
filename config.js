import mongoose from "mongoose";
import winston,{transports,createLogger} from "winston";
import jwt from "jsonwebtoken";
import { User } from "./src/model/user.model";
import bcrypt from "bcryptjs";

export const configureDatabase=()=>{
    var db_host="";
switch(process.env.NODE_ENV){
    case "prod":db_host=process.env.PROD_DB_HOST;break;
    case "test":db_host=process.env.TEST_DB_HOST;break;
    default:db_host=process.env.DEV_DB_HOST;break;
}
mongoose.connect(db_host,{useNewUrlParser:true,useUnifiedTopology: true,useFindAndModify: false, useCreateIndex: true},
        err=>err?
            console.log(err):
            console.log("Connected to DB: ",db_host));
}

const logger=new createLogger({
    format: winston.format.simple(),
    transports:[new transports.Console()]
});

export const logRequest=(req,res,next)=>{
        logger.info(`${req.method} ${req.url} | Time: ${(new Date()).toLocaleString()}`);
        next();
}
export const logError=(err,req,res,next)=>{
    logger.error(err);
    next(err);
}

export const auth=async(req,res,next)=>{
    const token = req.header("x-app-token");
    if (!token) {return res.status(401).send("Access Denied"); }
    try {
        const decodedToken = jwt.verify(token, process.env.TOKEN_SECRET);
        const user = await User.findById(decodedToken._id);
        if (!user) {
        return res.status(400).send("You need to be a User to see this route");
        }
        next();
    } catch (error) {
        res.status(400).send("Invalid Token");
    }
}

export const seedUsers=async()=>{
    await User.deleteMany({});
    const users=[
        new User({
            username:"user1",
            password:"somePassword1234"
        }),
        new User({
            username:"user2",
            password:"123456"
        })
    ]
    users.forEach(async user=>{
        const salt=await bcrypt.genSalt(10);
        const hashedPassword=await bcrypt.hash(user.password,salt);
        user.password=hashedPassword;
        await user.save();
    });
}