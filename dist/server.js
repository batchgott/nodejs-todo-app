"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require("express");

var _express2 = _interopRequireDefault(_express);

var _item = require("./src/routes/item.routes");

var _item2 = _interopRequireDefault(_item);

var _config = require("./config");

var _auth = require("./src/controller/auth.controller");

var AuthContoller = _interopRequireWildcard(_auth);

var _cors = require("cors");

var _cors2 = _interopRequireDefault(_cors);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

require('dotenv').config();

var app = (0, _express2.default)();

(0, _config.configureDatabase)();

app.use((0, _cors2.default)());
app.use(_express2.default.json());
app.use(_config.logRequest);
app.use(_config.logError);
(0, _config.seedUsers)();

var router = _express2.default.Router();
router.use("/items", _config.auth, _item2.default);
router.post("/login", AuthContoller.login);
app.use("/api", router);

app.listen(3000);
console.log('app running on port http://127.0.0.1:3000');

exports.default = app;