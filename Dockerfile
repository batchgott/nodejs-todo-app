FROM node:12.2.0

WORKDIR /app

ENV PATH /app/node_modules/.bin:$PATH

COPY package.json /app/package.json
RUN npm install


EXPOSE 3000

#CMD npm run start:hotreload

#create image:
#sudo docker build -t todoapp-backend-image:dev .

#start backend:
# sudo docker run -v ${PWD}:/app -p 3000:3000 todoapp-backend-image:dev

#start mongo:
#sudo docker run --name mongodb -e ALLOW_EMPTY_PASSWORD=yes -p 27027:27017 bitnami/mongodb:latest

#kill process of port 
#sudo lsof -ti tcp:3000 | sudo xargs kill
